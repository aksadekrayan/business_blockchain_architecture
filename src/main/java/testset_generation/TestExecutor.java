package testset_generation;



import architecture.p2pCommunication.CommunicationNodeManager;
import architecture.p2pCommunication.Message;
import architecture.p2pCommunication.Node;
import architecture.persistance.PublicationTransaction;
import architecture.persistance.Transaction;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import static testset_generation.TestGenerator.testPath;


public class TestExecutor {

    private Node nodeToRun;
    private ArrayList<Transaction> myTransactions;

    public static void main (String args[]){
        ArrayList<CommunicationNodeManager> nodeManagers=new ArrayList<>();
        TestExecutor executor = new TestExecutor();
        for (int i=1;i<=6;i++){
            nodeManagers.add((executor.runNode("node_"+i)));
        }
    }

    public CommunicationNodeManager runNode(String nodeName){
        CommunicationNodeManager nodeManager=null;
        try {
                //On récupère le noeud à partir du fichier sérialisé
                String nodeFile = (testPath+"/"+nodeName+".node");
                ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(nodeFile)) ;
                nodeToRun = (Node) ois.readObject();
                if(nodeToRun == null)
                {
                    System.err.print("Node file not found");
                }else {
                //On lance le noeud
                nodeManager = new CommunicationNodeManager(nodeToRun);
                nodeManager.startHost();
            }
        }catch (IOException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return nodeManager;
    }
}
