package testset_generation;

import architecture.consensus.ConsensusManager;
import architecture.p2pCommunication.Node;
import architecture.persistance.Block;
import architecture.persistance.Blockchain;
import architecture.persistance.PublicationTransaction;
import architecture.security.*;
import architecture.security.SecurityManager;
import springshell_client.SmartContractAdapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.PublicKey;
import java.util.*;

public class TestGenerator {


    private static final String nullString = null;
    private static final String genesisString = "genesis";
    private static final Block genesis = new Block(nullString,genesisString);
    private final static String walletsPath="src/main/resources/wallets";
    public final static String testPath="src/main/resources/testDir";

    private static Blockchain blockchain = new Blockchain();
    private static SecurityManager securityManager = new SecurityManager();
    private static ArrayList<Node> nodes;
    private static ArrayList<PublicationTransaction> transactions;

    private final static List<String> realMachinesNumber = new ArrayList<>();

    public static void main(String args[]){
        //addRealMachineAddress("192.168.1.14");
        //addRealMachineAddress("192.168.1.15");
        //addRealMachineAddress("192.168.1.16");
        generateTest(6,0,
                0,3,1);
        displayNodes();
        //displayTransactions();
    }


    public static void addRealMachineAddress(String address){
        realMachinesNumber.add(address);
    }

    public static void generateTest(  int numberOfNodes, //Nombre de noeuds à générer dans le jeu de test
                                      int numberOfTransactionsToLaunch, // Nombre de nouvelles transactions à lancer
                                      int numberOfPendingTransactions,  // Nombre de transactions en attente
                                      int averageCollaboratorsPerTransaction, //Nombre de collaborateurs par transaction
                                      int averagePeersPerNode){ //Nombre de pairs par noeud (Entrelacement)

        PKIAdapter pkiAdapter = PKIAdapterLauncher.launch();
        nodes = new ArrayList<>();
        transactions = new ArrayList<>();
        //Creer les noeuds
        for (int i = 1; i<=numberOfNodes ;i++){
            //Pour chaque noeud, lui créer une wallet, l'inscrire à la PKI et gérer ses interconnexions
            Node node;
            String name = "node_"+i;
            //On crée une wallet pour le noeud et on l'inscrit à la pki
            Wallet wallet =  pkiAdapter.signUp(name,"secret_"+name);
            //Si tous le jeu de test ne se fait pas sur la meme machine
            if (realMachinesNumber.size() != 0) {
                //On crée des noeuds suivant les adresses des machines réelles
                node = new Node(name, realMachinesNumber.get(i % realMachinesNumber.size()), 3100 + i, wallet, genesis);
            }else{
                //On crée des noeuds en faisant varier le port pour permettre l'execution sur la meme machine
                node = new Node(name, "localhost", 3100 + i, wallet, genesis);
            }
            nodes.add(node); //On ajoute le noeud à la structure à sérialiser
        }
        //Pour relier les noeuds entre eux on sélectionne aléatoirement un noeud à la fois
        Random random = new Random();
        ArrayList previousNodes = new ArrayList();
        //Pour chaque noeud à relier
        for (Node node:nodes
             ) {
                int cpt=0;
                //On récupère nombre aléatoire de noeuds à lui relier autour de la valeur moyenne indiquée en paramètre
                int peersNumber=random.nextInt(2*averagePeersPerNode-1)+1;
                ArrayList<Node> peerList = new ArrayList(); // La structure de pairs à relier au noeud
                Iterator<Node> it = previousNodes.iterator();
                Collections.shuffle(previousNodes); // On mélange la structure de noeuds pour avoir un next element aléatoire
                while (cpt<=peersNumber && it.hasNext()){
                    //On crée une instance réduite d'un noeud avec l'adresse et le port qui sont les attributs nécessaires
                    //à la communication p2p
                    Node nextNode = it.next();
                    Node nodeToAdd = new Node(nextNode.getAddress(),nextNode.getPort());
                    //On ajoute le noeud créé en tant que pair
                    peerList.add(nodeToAdd);
                    cpt++;
                }
                node.setPeers(peerList);
                previousNodes.add(node);
        }

        //Creer les transactions
        for (int i = 1; i<=numberOfTransactionsToLaunch ;i++){
            //Pour chaque transaction, la créer, y ajouter les collaborateurs selon le nombre moyen de
            //collaborateurs par transaction et l'affecter aux noeuds qui devront la lancer
            //ou la rajouter aux listes de transactions en attente
            //On crée le contrat sous une variable String pour les besoins du prototype
            String contract = "contract_"+i;
            contract= contract+"\n"+SmartContractAdapter.loadFromFile("smartcontract.orcha");
            //On récupère un noeud aléatoire qui sera le client de la collaboration, celui qui lance la transaction
            Node sender = nodes.get(random.nextInt(nodes.size()));
            //On crée la transaction
            PublicationTransaction transaction = new PublicationTransaction(sender.getName(),sender.getWallet().getPublicKey(),contract);
            transaction.addCollaborator(sender.getWallet().getPublicKey());
            //On génère un chiffre aléatoire autour de la moyenne du nombre de collaborateurs par collaboration
            int collabNumber = random.nextInt(averageCollaboratorsPerTransaction*2-1)+1;
            //On crée une copie mélangée de la liste des noeuds pour obtenir une suite d'élements uniques aléatoires
            ArrayList<Node> shuffledNodes = new ArrayList<Node>(nodes);
            Collections.shuffle(shuffledNodes);

            if (collabNumber >= numberOfNodes)
                collabNumber = numberOfNodes-1;
            //On boucle pour chacun des collaborateurs à rajouter
            for (int j = 1; j<=collabNumber ; j++){
                //On récupère le noeud suivant aléatoire
                Node collab = shuffledNodes.get(j);
                //On le rajoute à la collaboration
                transaction.addCollaborator(collab.getWallet().getPublicKey());
            }
            //On affecte la collaboration à la transaction crée

            transaction.setSenderSignature(SecurityManager.sign(transaction.calculateHash(),sender.getWallet().getPrivateKey()));
            transaction.addCollaboratorSignature(sender.getWallet().getPublicKey(),sender.getWallet().getPrivateKey());
            transactions.add(transaction);
            //On affecte les transactions à chacun des noeuds qui y sont concernés :
            // Pour chaque noeud,
            for (Node n:nodes
                 ) {
                //Si le noeud apparait comme sender, (client) dans la transaction
                if (transaction.getSenderAddress().equals(n.getWallet().getPublicKey())){
                    //Ajouter la transaction à l'ensemble de transactions à lancer du noeud
                    n.addConcernedTransaction(transaction);
                }
            }
        }

        //Pour les transactions en attente il s'agit du même processus de génération
        //A l'exception de l'ajout à la liste des transactions en attente au niveau de tous les noeuds
        // et non à la liste des transactions à lancer du noeud sender.
        for (int i = 1; i<=numberOfPendingTransactions ;i++){
        //ou la rajouter aux listes de transactions en attente
            String contract = "contract_"+i;
            ArrayList<Node> collaborateurs = new ArrayList<Node>();
            Node sender = nodes.get(random.nextInt(nodes.size()));
            PublicationTransaction transaction = new PublicationTransaction(sender.getName(), sender.getWallet().getPublicKey(),contract);
            transaction.addCollaborator(sender.getWallet().getPublicKey());
            int collabNumber = random.nextInt(averageCollaboratorsPerTransaction*2-1)+1;
            ArrayList<Node> shuffledNodes = new ArrayList<Node>(nodes);
            Collections.shuffle(shuffledNodes);
            if (collabNumber >= numberOfNodes)
                collabNumber = numberOfNodes-1;
            for (int j = 1; j<=collabNumber ; j++){
                Node collab = shuffledNodes.get(j);
                collaborateurs.add(collab);
                transaction.addCollaborator(collab.getWallet().getPublicKey());
            }
            for (Node collab:collaborateurs
                 ) {
                transaction.addCollaboratorSignature(collab.getWallet().getPublicKey(),collab.getWallet().getPrivateKey());
            }
            transaction.setSenderSignature(SecurityManager.sign(transaction.calculateHash(),sender.getWallet().getPrivateKey()));
            transaction.addCollaboratorSignature(sender.getWallet().getPublicKey(),sender.getWallet().getPrivateKey());
            transactions.add(transaction);
            for (Node n:nodes
                 ) {
                n.getPendingTransactions().add(transaction);
            }
        }
        saveTestInFile();
    }

    private static void displayNodes (){
        for (Node n:nodes
                ) {
            System.out.println(n.toString());
        }
    }

    public static void saveTestInFile(){

        //System.out.println(new Gson().toJson(nodes.get(46)));

        try {
            for (Node n : nodes
                    ) {
                File fout = new File(testPath+"/"+n.getName()+".node");
                ObjectOutputStream oos = null;
                oos = new ObjectOutputStream(new FileOutputStream(fout));
                oos.writeObject(n);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private static void displayTransactions() {
        for (PublicationTransaction t:transactions
             ) {
            System.out.println("transaction "+transactions.indexOf(t)+" with "+t.getSmartContract()+" has "+t.getCollaborateurs().size() +" collaborators");
        }
    }

}
