package logging;


import architecture.p2pCommunication.Node;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.boot.ansi.AnsiOutput;
import springshell_client.ANSI_COLOR;

import java.util.Calendar;

public class LoggingManager {

    private static Calendar calendar;
    private static String timeStamp;

    public static void log(String s){
        calendar= Calendar.getInstance();
        timeStamp= calendar.get(Calendar.DAY_OF_MONTH)+"/"+calendar.get(Calendar.MONTH)+
                "/"+calendar.get(Calendar.YEAR) + "||"+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE)+
                ":"+calendar.get(Calendar.SECOND) +":"+calendar.get(Calendar.MILLISECOND);
        System.out.println(timeStamp+ ">"+s);
    }

    public static void log(String s, Node node){
        calendar= Calendar.getInstance();
        timeStamp= ANSI_COLOR.YELLOW + calendar.get(Calendar.DAY_OF_MONTH)+"/"+calendar.get(Calendar.MONTH)+
                "/"+calendar.get(Calendar.YEAR) + "||"+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE)+
                ":"+calendar.get(Calendar.SECOND) +":"+calendar.get(Calendar.MILLISECOND);
        String nodeTrace = fill(ANSI_COLOR.CYAN +node.getName())+ANSI_COLOR.RESET+"@"+
                ANSI_COLOR.BLUE+node.getAddress()+ANSI_COLOR.RESET+":"+ANSI_COLOR.BLUE+node.getPort()+ANSI_COLOR.RESET;
        System.out.println(timeStamp+" "+nodeTrace+ ">"+s);

    }

    private static String fill(String s){
        int fillVal = 10;   //Majorant de la longueur des noms des noeuds
        String result = s;
        int toFill = fillVal-s.length();
        String spaces;
        if (toFill > 0)
            spaces = new String(new char[toFill]).replace('\0', ' ');
        else
            spaces ="";
        return  spaces+result;
    }

    public static void logError(String s,Node node) {
        calendar= Calendar.getInstance();
        timeStamp= calendar.get(Calendar.DAY_OF_MONTH)+"/"+calendar.get(Calendar.MONTH)+
                "/"+calendar.get(Calendar.YEAR) + "||"+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE)+
                ":"+calendar.get(Calendar.SECOND) +":"+calendar.get(Calendar.MILLISECOND);
        String nodeTrace = fill(node.getName())+"@"+node.getAddress()+":"+node.getPort();
        System.err.println(timeStamp+" "+nodeTrace+ ">"+s);
    }

    public static void logError(String s){
        calendar= Calendar.getInstance();
        timeStamp= calendar.get(Calendar.DAY_OF_MONTH)+"/"+calendar.get(Calendar.MONTH)+
                "/"+calendar.get(Calendar.YEAR) + "||"+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE)+
                ":"+calendar.get(Calendar.SECOND) +":"+calendar.get(Calendar.MILLISECOND);
        System.err.println(timeStamp+ ">"+s);
    }
}
