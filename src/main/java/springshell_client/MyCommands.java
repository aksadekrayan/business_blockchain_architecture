package springshell_client;

/**
 * Created by bcs on 11/05/2018.
 */

import architecture.p2pCommunication.Node;
import architecture.persistance.PublicationTransaction;
import architecture.persistance.Transaction;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.ArrayList;
import java.util.List;

@ShellComponent
public class MyCommands {

    NodeManager nodeManager = new NodeManager();

    /*********************************** Configuration du noeud *******************************************/

    @ShellMethod(value = "Lancer le noeud\n", group = "commandes de configuration du noeud")
    public void StartNode(String nodeIdentity) {
        nodeManager.startNode(nodeIdentity);
    }

    @ShellMethod(value = "Arreter le noeud\n", group = "commandes de configuration du noeud")
    public void CloseNode() {
        nodeManager.stopNode();
    }

    @ShellMethod(value = "Génération d'un wallet au noeud\n           -wallet file\n", group = "commandes de configuration du noeud")
    public void ConfigUserWallet( ) {
        nodeManager.generateWallet();
    }

    @ShellMethod(value = "Decouverte des noeuds pairs\n", group = "commandes de configuration du noeud")
    public List<Node> ConfigDiscoverPeers() {
        List<Node> peers = new ArrayList<>();
        return peers;
    }

    @ShellMethod(value = "Ajout d'un nouveau pair à la liste du noeud\n           -Peer ip adresse -Peer port\n", group = "commandes de configuration du noeud")
    public void ConfigAddPeer(String ipadresse ,  int port ) {
        nodeManager.addPeer(ipadresse, port);
    }

    /********************* traitement des transactions de création de smart contrats ************************/

    @ShellMethod(value = "Creation d'une transaction\n           -Transaction name - smart contract file\n", group = "Publication d'un smart-contrat")
    public void CreateSC(String nameTransaction, String codeSC) {
        nodeManager.createSmartContract(nameTransaction, codeSC);
    }

    @ShellMethod(value = "Ajout d'un collaborateur à un smart-contrat \n           -Transaction name -collaborator role - collaborator public key\n", group = "Publication d'un smart-contrat")
    public void AddCollab(String nameTransaction, String role, String identity) {
        nodeManager.addCollabToSC(nameTransaction, role, identity);
    }

    @ShellMethod(value = "Signer une transaction de création de smart-contrat\n           -Transaction name\n", group = "Publication d'un smart-contrat")
    public Transaction SignSC(String nameTransaction) {

        return new PublicationTransaction("", null, null);
    }

    @ShellMethod(value = "Diffusion d'une transaction de création de smart-contrat\n           -Transaction name\n", group = "Publication d'un smart-contrat")
    public void BroadcastSC(String nameTransaction) {
        nodeManager.broadcastSC(nameTransaction);
    }


    /*************************************Traitement des Blocs *********************************************/

    @ShellMethod(value = "Création d'un nouveau bloc\n           -Bloc name\n", group = "Validation d'un bloc")
    public void CreateB(String nameBloc) {


    }

    @ShellMethod(value = "Signer un nouveau bloc deja créé \n           -Bloc name\n", group = "Validation d'un bloc")
    public void SignB(String nameBloc) {

    }

    @ShellMethod(value = "Création d'un nouveau bloc \n           -Bloc name\n", group = "Validation d'un bloc")
    public void BroadcastB(String nameBloc) {

    }

    /*************************************    Affichages  *********************************************/

    @ShellMethod(value = "Afficher la vesrion de la Blockchain du noeud\n", group = "Affichages")
    public void displayBlockchainV() {
        nodeManager.displayBlockchainVersion();
    }


    @ShellMethod(value = "Afficher les transactions valides en attente\n", group = "Affichages")
    public void DisplayPendingT(){

    }

    @ShellMethod(value = "Afficher la liste des noeuds pairs\n", group = "Affichages")
    public void DisplayPeers(){
        nodeManager.displayPeers();
    }

    @ShellMethod(value = "Afficher les transactions publiées par le noeud\n", group = "Affichages")
    public void DisplayNodeT(){
        nodeManager.displayNodeTransactions();
    }

    /********************************* Commandes de test **********************************************/

    @ShellMethod(value = "Lancer un noeud à partir d'un jeu de test généré \n", group = "Tests")
    public void StartTestNode(String node){
        nodeManager.startTestNode(node);
    }

    @ShellMethod(value = "Lancer un noeud à partir d'un jeu de test généré \n", group = "Tests")
    public void SendTestTransaction(){
        nodeManager.sendTestTransaction();
    }

    @ShellMethod(value = "Simule le top d'horloge qui désigne le noeud comme validateur \n", group = "Tests")
    public void ProcessNomination(){
        nodeManager.processNomination();
    }


}
