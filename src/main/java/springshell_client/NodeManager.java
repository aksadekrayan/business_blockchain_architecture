package springshell_client;

import architecture.consensus.ConsensusManager;
import architecture.security.FirebasePKIAdapter;
import architecture.security.PKIAdapterLauncher;
import com.google.gson.Gson;
import logging.LoggingManager;
import architecture.p2pCommunication.CommunicationNodeManager;
import architecture.p2pCommunication.Node;
import architecture.persistance.Block;
import architecture.persistance.PublicationTransaction;
import architecture.persistance.Transaction;
import architecture.security.PKIAdapter;
import architecture.security.Wallet;
import testset_generation.TestExecutor;

import java.io.*;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PublicKey;
import java.util.*;

import static architecture.p2pCommunication.Message.MESSAGE_TYPE.SEND_NEW_BLOCK;
import static architecture.p2pCommunication.Message.MESSAGE_TYPE.SEND_NEW_TRANSACTION;


/**
 * Created by bcs on 12/05/2018.
 */
public class NodeManager {


    public static final String nullString = null;
    public static final String genesisString = "genesis";
    private static final String myWalletPath = "src/main/resources/myWallet.ser";
    private static final String peerListPath = "src/main/resources/peerList.ser";
    public static final Block genesis = new Block(nullString,genesisString);

    public static CommunicationNodeManager myNode  = null;
    ArrayList<Node> listePeers = new ArrayList<>();
    List<Transaction> liste = new ArrayList<>();
    Wallet walletNode = null;
    int cpt = 0;


    List<PublicationTransaction> nodeTransactions = new ArrayList<>();
    Map< String, PublicationTransaction> nodeSCs = new HashMap<>();


    /******************************** Opérations du configuration du noeud ******************************/

    // pour la commande start node
    public void startNode(String nodeIdentity)
    {
        File fichier =  new File(myWalletPath),fichier_peers = new File(peerListPath);
        if(fichier.exists())
        {
            try {
                ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichier)) ;
                walletNode = (Wallet)ois.readObject() ;
                ois.close();
                if(walletNode != null)
                {
                    try {
                        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fichier_peers));
                        listePeers = (ArrayList<Node>) objectInputStream.readObject();
                        objectInputStream.close();
                    }catch (EOFException e){
                        listePeers= new ArrayList<>();
                    }
                    if (listePeers == null)
                        listePeers= new ArrayList<>();

                    myNode = new CommunicationNodeManager(nodeIdentity,InetAddress.getLocalHost().getHostAddress(),3000,walletNode,genesis,listePeers);
                    myNode.startHost();
                }else
                {
                    LoggingManager.logError("Wallet doesn't exist, configure one to your node ");
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }else
        {
            LoggingManager.logError("Wallet doesn't exist, configure one to your node ");
        }

    }

    //pour la commande stop node
    public void stopNode() {
        myNode.stopHost();
        LoggingManager.log("Node closed",myNode.getNode());
    }


    //pour la commande config user wallet
    public void generateWallet()
    {

        PKIAdapter adapter = PKIAdapterLauncher.launch();
        Scanner scanner = new Scanner(System.in);
        String identity="", secret="";
        System.out.print(" Please input the identity shared with PKI > ");
        identity = scanner.nextLine();
        System.out.print("\n Please input the secret linked with your identity shared with PKI > ");
        secret = scanner.nextLine();
        System.out.print("\n");
        Wallet myWallet = adapter.signUp(identity,secret);
        LoggingManager.log(" Wallet \""+identity+"\" generated and registered in the PKI.");

        File fout = new File(myWalletPath);
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(fout));
            oos.writeObject(myWallet);
        }catch (IOException e){
            e.printStackTrace();
        }

        /*File fichierPubKey =  new File("wallet\\myPublicKey.ser");
        oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(fichierPubKey));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            oos.writeObject(myWallet.getPublicKey()) ;
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    //pour la commande config add peer @ip port
    public void addPeer(String ipAdresse, int port)
    {
        Node newPeer = new Node(ipAdresse, port);
        listePeers.add(newPeer);

        File fout = new File(peerListPath);
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(fout));
            oos.writeObject(listePeers);
        }catch (IOException e){
            System.err.println("Failed to save PeerList to File");
            e.printStackTrace();
        }

        LoggingManager.log(" node @ip : "+ ipAdresse+ " port : "+port + " successfully added ! ");
    }

    /********************************          Opérations d'affichage          ******************************/

    // pour la commande log blockchain version
    public void displayBlockchainVersion()
    {
       myNode.getNode().getBlockchain().afficherBlockchain();
    }

    // pour la commande log user transactions
    public void displayNodeTransactions()
    {
        nodeSCs.forEach((k, v) -> {
            System.out.println(k+" [");
            System.out.println("\tsmart-contrat  code : {"+ v.getSmartContract().toString()+ "}");
            System.out.println("\ttimeStamp: "  + v.getTimestamp());
            System.out.println(" ]");
        });
    }
    /********************************          Opérations de transactions        ****************************/

    //pour la commande create-sc
    public void createSmartContract(String name, String smartContractCode)
    {
        if (myNode != null) {

                PublicationTransaction t = new PublicationTransaction(myNode.getNode().getName(), walletNode.getPublicKey(),
                        SmartContractAdapter.loadFromFile(smartContractCode));
                t.addCollaborator(myNode.getNode().getWallet().getPublicKey());
                nodeSCs.put(name, t);
                LoggingManager.log(" Smart contract transaction " + name + " successfully created ! ",myNode.getNode());

        }else{
            LoggingManager.logError(" Please launch node before creating a smart-contract");
        }
    }

    public void addCollabToSC(String nameTransaction, String role, String collabIdentity) {
        PKIAdapter pkiAdapter = PKIAdapterLauncher.launch();
        PublicKey publicKey = pkiAdapter.getPublicKey(collabIdentity);
        if (publicKey != null){
            PublicationTransaction t = nodeSCs.get(nameTransaction);
            t.addCollaborator(publicKey);
            LoggingManager.log(" Collaborator "+collabIdentity+" added to transaction "+nameTransaction,myNode.getNode());
        }else{
            LoggingManager.logError(" Error: collaborator "+collabIdentity+" is not registered in the pki",myNode.getNode());
        }
    }

    public void startTestNode(String node) {
        TestExecutor executor = new TestExecutor();
        myNode = executor.runNode(node);
    }

    public void sendTestTransaction() {
        PublicationTransaction transaction = (PublicationTransaction) myNode.getNode().getConcernedTransactions().get(cpt);
        myNode.broadcast(SEND_NEW_TRANSACTION,transaction);
        cpt++;
    }

    public void processNomination() {
        ConsensusManager consensusManager = new ConsensusManager();
        Block blockToSend = consensusManager.processNomination(myNode.getNode());
        if (blockToSend != null) {
            if (!blockToSend.equals(genesis)) {
                LoggingManager.log(" Validator turn detected", myNode.getNode());
                myNode.broadcast(SEND_NEW_BLOCK, blockToSend);
            }//else
            //LoggingManager.log(" No pending transactions to add to Block",nodeManager.getNode());
        }
    }

    public void broadcastSC(String nameTransaction) {
        if (nodeSCs.containsKey(nameTransaction)) {
            PublicationTransaction transaction = nodeSCs.get(nameTransaction);
            Wallet wallet = myNode.getNode().getWallet();
            transaction.addCollaboratorSignature(wallet.getPublicKey(),wallet.getPrivateKey());
            transaction.setSenderSignature(transaction.generateSignature(wallet.getPrivateKey()));
            LoggingManager.log(" Broadcasting transaction "+nameTransaction);
            myNode.broadcast(SEND_NEW_TRANSACTION,transaction);
        }else{
            LoggingManager.logError(" smart-contract "+nameTransaction+" doesn't exist", myNode.getNode());
        }
    }

    public void displayPeers() {
        List<Node> peerList= myNode.getNode().getPeers();

        for (Node n:peerList
             ) {
            System.out.println("peer:"+n.getAddress()+":"+n.getPort());
        }
    }
}
