package springshell_client;

import org.codehaus.groovy.tools.shell.IO;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SmartContractAdapter {

    private final static String SCPath = "src/main/resources/";

    public static String loadFromFile(String fileName){
        String smartcontract="";
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(SCPath + fileName));
            smartcontract= new String(encoded, Charset.defaultCharset());
        }catch (IOException e){
            System.err.println("Error loading smart-contract code from file");
        }
        return smartcontract;
    }
}
