package architecture;

import architecture.consensus.ConsensusManager;
import architecture.security.PKIAdapter;
import architecture.security.PKIAdapterLauncher;
import architecture.security.SecurityManager;
import springshell_client.SmartContractAdapter;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Random;

public class Main {
    public static void main(String args[]){

        //System.out.println(SmartContractAdapter.loadFromFile("smartcontract.orcha"));
        PKIAdapter pkiAdapter = PKIAdapterLauncher.launch();
        PublicKey publicKey = pkiAdapter.getPublicKey("TRANSPORTEUR");
        System.out.println(publicKey);
    }
}
