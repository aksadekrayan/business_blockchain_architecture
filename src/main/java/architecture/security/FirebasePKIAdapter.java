package architecture.security;

import architecture.consensus.ConsensusManager;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;

import java.util.LinkedHashMap;

public class FirebasePKIAdapter extends PKIAdapter {

    public static void main(String args[]){

        PKIAdapter pkiAdapter = PKIAdapterLauncher.launch();
        try{
            System.in.read();
        }catch (Exception e){
            e.printStackTrace();
        }
        pkiAdapter.saveList();

    }

    public FirebasePKIAdapter(){
        String decoded = "";
        HttpClient httpClient = HttpClients.createDefault();
        HttpGet getRequest = new HttpGet(
                "https://pki-test-b375a.firebaseio.com/.json");
        getRequest.addHeader("accept", "application/json");

        try {
            HttpResponse response = httpClient.execute(getRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            decoded = IOUtils.toString(response.getEntity().getContent());
        }catch (Exception e){
            System.err.println("Connexion problem, please try again");
            //e.printStackTrace();
        }
        if (decoded.equals("")){
            this.pkiMap = new LinkedHashMap();
        }else {
            loadListFromJson(decoded);
        }
    }


    @Override
    public void saveList() {
        HttpResponse response = null;
        String data = exportListToJson();
        HttpClient httpClient = HttpClients.createDefault();
        HttpPatch patch = new HttpPatch("https://pki-test-b375a.firebaseio.com/.json");
        try {
            StringEntity params =new StringEntity(data);
            patch.setEntity(params);
            response = httpClient.execute(patch);
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
