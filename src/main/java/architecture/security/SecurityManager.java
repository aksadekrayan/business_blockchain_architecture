package architecture.security;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Base64;
import java.util.Random;

public class SecurityManager {

    // Crypter un message 'valueEnc' avec la clé symétrique 'secKey'
    public static String encrypt(final String valueEnc, final String secKey) {
        String encryptedVal = null;
        try {
            final Key key = generateKeyFromString(secKey);
            final Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.ENCRYPT_MODE, key);
            final byte[] encValue = c.doFinal(valueEnc.getBytes());
            encryptedVal = new BASE64Encoder().encode(encValue);
        } catch(Exception ex) {
            System.err.println("Error with symetric encryption");
            ex.printStackTrace();
        }
        return encryptedVal;
    }
    public static String encrypt(final String valueEnc, final Key key) {
        String encryptedVal = null;
        try {
            final Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.ENCRYPT_MODE, key);
            final byte[] encValue = c.doFinal(valueEnc.getBytes());
            encryptedVal = new BASE64Encoder().encode(encValue);
        } catch(Exception ex) {
            System.err.println("Error with symetric encryption");
            ex.printStackTrace();
        }
        return encryptedVal;
    }
    // Crypter un chiffré 'encryptedValue' avec la clé symétrique 'secretKey'
    public static String decrypt(final String encryptedValue, final String secretKey) {
        String decryptedValue = null;
        try {
            final Key key = generateKeyFromString(secretKey);
            final Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.DECRYPT_MODE, key);
            final byte[] decorVal = new BASE64Decoder().decodeBuffer(encryptedValue);
            final byte[] decValue = c.doFinal(decorVal);
            decryptedValue = new String(decValue);
        } catch(Exception ex) {
            System.err.println("Error with symetric decryption");
            ex.printStackTrace();
        }
        return decryptedValue;
    }
    public static String decrypt(final String encryptedValue, final Key key) {
        String decryptedValue = null;
        try {
            final Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.DECRYPT_MODE, key);
            final byte[] decorVal = new BASE64Decoder().decodeBuffer(encryptedValue);
            final byte[] decValue = c.doFinal(decorVal);
            decryptedValue = new String(decValue);
        } catch(Exception ex) {
            System.err.println("Error with symetric decryption");
            ex.printStackTrace();
        }
        return decryptedValue;
    }

    //Deriver une clé à partir du mot clé secKey
    public static Key generateKeyFromString(final String secKey){
        final byte[] keyVal;
        try {
            keyVal = new BASE64Decoder().decodeBuffer(secKey);
            final Key key = new SecretKeySpec(keyVal, "AES");
            return key;
        } catch (IOException e) {
            System.err.println("Error in key generation from string");
            e.printStackTrace();
            return null;
        }
    }

    // Crypter un message 'plainText' avec la clé publique 'publicKey'
    public static String encrypt(String plainText, PublicKey publicKey) {
        byte[] cipherText =null;
        try {
            Cipher encryptCipher = Cipher.getInstance("RSA");
                encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            cipherText = encryptCipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));

        } catch (Exception e) {
            System.err.println("Problem with the encryption");
            e.printStackTrace();
        }
        return Base64.getEncoder().encodeToString(cipherText);
    }

    //Decrypter un message 'cipherText' avec la clé privée 'privateKey'
    public static String decrypt(String cipherText, PrivateKey privateKey)  {
        byte[] bytes = Base64.getDecoder().decode(cipherText);
        try {
            Cipher decriptCipher = Cipher.getInstance("RSA");
            decriptCipher.init(Cipher.DECRYPT_MODE, privateKey);

            return new String(decriptCipher.doFinal(bytes), StandardCharsets.UTF_8);
        }
        catch (Exception e){
            System.err.println("Error decrypting");
            e.printStackTrace();
            return null;
        }
    }

    // Signer un message 'plainText' avec la clé privée 'privateKey'
    public static String sign(String plainText, PrivateKey privateKey)  {
        Signature privateSignature = null;
        byte[] signature=null;
        try {
            privateSignature = Signature.getInstance("SHA256withRSA");

            privateSignature.initSign(privateKey);
            privateSignature.update(plainText.getBytes(StandardCharsets.UTF_8));

            signature = privateSignature.sign();

        } catch (Exception e) {
            System.out.println("Error : Signature failed");
            e.printStackTrace();
        }

        return Base64.getEncoder().encodeToString(signature);
    }

    //Vérifier la validité de la signature 'signature' du message 'plainText' à l'aide de la clé publique 'publicKey'
    public static boolean verifySignature(String plainText, String signature, PublicKey publicKey) {
        try {
            Signature publicSignature = Signature.getInstance("SHA256withRSA");
            publicSignature.initVerify(publicKey);
            publicSignature.update(plainText.getBytes(StandardCharsets.UTF_8));

            byte[] signatureBytes = Base64.getDecoder().decode(signature);
            return publicSignature.verify(signatureBytes);
        }catch (Exception e){
            System.out.println("Error : Signature verification failed");
            e.printStackTrace();
            return false;
        }
    }
    public static Key generateSymetricKey(){
        String keyString = randomString(32);
        return generateKeyFromString(keyString);
    }

    public static String randomString(int length) {
        char[] CHARSET_AZ_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        Random random = new SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            // picks a random index out of character set > random character
            int randomCharIndex = random.nextInt(CHARSET_AZ_09.length);
            result[i] = CHARSET_AZ_09[randomCharIndex];
        }
        return new String(result);
    }

}
