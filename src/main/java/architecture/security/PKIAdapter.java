package architecture.security;

import com.google.gson.Gson;
import sun.security.rsa.RSAPublicKeyImpl;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by SNTF on 16/05/2018.
 */
public abstract class PKIAdapter {

    protected static Map pkiMap = new LinkedHashMap();

    protected String exportListToJson(){
        return new Gson().toJson(pkiMap);
    }

    protected void loadListFromJson(String json){
        this.pkiMap = new Gson().fromJson(json,LinkedHashMap.class);
    }

    public Wallet signUp(String identity,String secret){
        Wallet w=null;

        if (verifySecret(identity,secret)) {
            w = new Wallet();
            pkiMap.put(identity, new Gson().toJson(w.getPublicKey()));
            saveList();
        }

        return w;
    }

    public PublicKey getPublicKey(String identity){
        try{
            return new Gson().fromJson(pkiMap.get(identity).toString(), RSAPublicKeyImpl.class);
        }catch (NullPointerException e){
            return null;
        }
    }

    public int getToken(String identity){
        List list = new ArrayList(pkiMap.keySet());
        return list.indexOf(identity);
    }

    public int getToken(PublicKey publicKey){
        List list = new ArrayList(pkiMap.keySet());
        for (Object id:pkiMap.keySet()
                ) {
            if(new Gson().fromJson(pkiMap.get(id).toString(), RSAPublicKeyImpl.class).equals(publicKey))
                return list.indexOf(id);
        }
        return -1;
    }

    @Override
    public String toString() {
        String resultat = "PKIAdapter{\n";
        for (Object element:pkiMap.keySet()
                ) {
            resultat = resultat + " "+element.toString()+" " + pkiMap.get(element).toString()+"\n";
        }
        resultat = resultat + "\n}";
        return resultat;
    }

    public int getValidatorsNumber() {
        return pkiMap.size();
    }


    protected boolean verifySecret(String identity, String secret) {
        return true;
    }

    abstract public void saveList();

}
