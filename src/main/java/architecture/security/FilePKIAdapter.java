package architecture.security;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;

/**
 * Created by SNTF on 16/05/2018.
 */
public class FilePKIAdapter extends PKIAdapter {


    private final static String filePath="src/main/resources/pki.json";
    private final static String walletsPath="src/main/resources/wallets";


    public FilePKIAdapter(){
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(filePath));
            String decoded = new String(encoded, Charset.defaultCharset());
            if (decoded.equals("")){
                this.pkiMap = new LinkedHashMap();
            }else {
                loadListFromJson(decoded);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void saveList(){
        byte[] data = exportListToJson().getBytes();
        Path file = Paths.get(filePath);
        try {
            Files.write(file, data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
