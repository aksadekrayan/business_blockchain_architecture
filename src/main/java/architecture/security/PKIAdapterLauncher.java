package architecture.security;

public class PKIAdapterLauncher {

    public static PKIAdapter launch(){
        return new FirebasePKIAdapter();
    }
}
