package architecture.security;

import groovy.util.MapEntry;

import java.io.Serializable;
import java.security.Key;
import java.security.PublicKey;
import java.util.Map;

public class CollabKeyDisposer implements Serializable {

    private String collabStringKey;
    private Key collabKey;

    public CollabKeyDisposer(){
        collabStringKey = SecurityManager.randomString(32);
        collabKey = SecurityManager.generateKeyFromString(collabStringKey);
    }

    public String encryptWithCollabKey(String message){
        return SecurityManager.encrypt(message,collabKey);
    }

    public Map.Entry<PublicKey,String> generateCollabEntry(PublicKey collaboratorKey){
        return new MapEntry(collaboratorKey,SecurityManager.encrypt(collabStringKey,collaboratorKey));
    }
}
