package architecture.p2pCommunication;

import architecture.consensus.ConsensusManager;
import logging.LoggingManager;
import architecture.persistance.Block;
import architecture.persistance.Transaction;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import static architecture.p2pCommunication.Message.MESSAGE_TYPE.*;


public class NodeServerThread extends Thread {
    private Socket client;
    private final CommunicationNodeManager nodeManager;
    private final ConsensusManager consensusManager = new ConsensusManager();

    NodeServerThread(final CommunicationNodeManager nodeManager, final Socket client) {
        super(nodeManager.getNode().getName() + System.currentTimeMillis());
        this.nodeManager = nodeManager;
        this.client = client;
    }

    @Override
    public void run() {
        Transaction transactionToSend=null;
        try (
                ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
                final ObjectInputStream in = new ObjectInputStream(client.getInputStream())) {
            Message message = new Message.MessageBuilder().withSender(nodeManager.getNode().getAddress(),nodeManager.getNode().getPort())
                    .withReceiver(client.getInetAddress().getHostAddress().toString(),client.getPort()).withType(READY).build();
            out.writeObject(message);
            Object fromClient;
            while ((fromClient = in.readObject()) != null) {
                if (fromClient instanceof Message) {
                    final Message msg = (Message) fromClient;
                    LoggingManager.log(String.format(" Received: %s", fromClient.toString()),nodeManager.getNode());
                    Node a = new Node(msg.senderHost,msg.senderPort);
                    nodeManager.getNode().addPeer(a);
                    if (SEND_NEW_BLOCK == msg.type) {
                        if (msg.getBlock()==null){
                            LoggingManager.log(" Recieved invalid Block from "+fromClient.toString(),nodeManager.getNode());
                        }
                            LoggingManager.log(" Processing recieved Block",nodeManager.getNode());
                            if (consensusManager.processBlock(nodeManager.getNode(),msg.getBlock())) {
                                LoggingManager.log(" Successfully added Block",nodeManager.getNode());
                                nodeManager.broadcast(SEND_NEW_BLOCK,msg.getBlock());
                            }
                        break;
                    } else if (REQUEST_BLOCKCHAIN == msg.type) {
                        out.writeObject(new Message.MessageBuilder()
                                .withSender(nodeManager.getNode().getAddress(),nodeManager.getNode().getPort())
                                .withType(RSPONSE_BLOCKCHAIN)
                                .withBlockchain(nodeManager.getNode().getBlockchain())
                                .withReceiver(msg.senderHost,msg.senderPort)
                                .build());
                        break;
                    } else if (SEND_NEW_TRANSACTION == msg.type){
                        transactionToSend = consensusManager.processTransaction(nodeManager.getNode(),msg.getTransaction());
                        if (transactionToSend.getNextMessageType()!=null)
                            nodeManager.broadcast(transactionToSend.getNextMessageType(),transactionToSend);
                        break;
                    } else if (SEND_SIGNED_TRANSACTION == msg.type){
                        transactionToSend = consensusManager.addToPendingTransactions(nodeManager.getNode(),msg.getTransaction());
                        if (transactionToSend.getNextMessageType()!=null)
                            nodeManager.broadcast(transactionToSend.getNextMessageType(),transactionToSend);
                        break;
                    } else if (SEND_VALIDATOR_NOMINATION == msg.type){
                        Block blockToSend = consensusManager.processNomination(nodeManager.getNode());
                        if ( blockToSend != null)
                            nodeManager.broadcast(SEND_NEW_BLOCK,blockToSend);
                        break;
                    }

                }
            }
            client.close();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }
}
