package architecture.p2pCommunication;

import architecture.persistance.Block;
import architecture.persistance.Blockchain;
import architecture.persistance.Transaction;

import java.io.Serializable;
import java.util.List;

public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    int senderPort;
    int receiverPort;
    String senderHost;
    String receiverHost;

    Message.MESSAGE_TYPE type;
    List<Block> blockOlds;
    Blockchain blockchain;
    Block block;
    Transaction transaction;

    public Transaction getTransaction() {
        return transaction;
    }
    public Blockchain getBlockchain() { return blockchain; }
    public Block getBlock() {return block;}

    public enum MESSAGE_TYPE {
        READY, REQUEST_BLOCKCHAIN, RSPONSE_BLOCKCHAIN,SEND_NEW_TRANSACTION,
        SEND_SIGNED_TRANSACTION, SEND_NEW_BLOCK, SEND_VALIDATOR_NOMINATION;
    }

    @Override
    public String toString() {
        if (blockchain != null){
            return String.format("Message {type=%s, sender=%s:%d, receiver=%s:%d, data=%s}", type, senderHost,senderPort,
                    receiverHost,receiverPort,blockchain.getStringResume());
        }else if (transaction != null){
            return String.format("Message {type=%s, sender=%s:%d, receiver=%s:%d, data=%s}", type, senderHost,senderPort,
                    receiverHost,receiverPort,transaction.toString());
        }else{
            return String.format("Message {type=%s, sender=%s:%d, receiver=%s:%d, data=NULL}", type, senderHost,senderPort,
                    receiverHost,receiverPort);
        }

    }

    static class MessageBuilder {
        private final Message message = new Message();

        Message.MessageBuilder withSender(final String host,final int port) {
            message.senderHost = host;
            message.senderPort = port;
            return this;
        }

        Message.MessageBuilder withReceiver(final String host,final int port) {
            message.receiverHost = host;
            message.receiverPort = port;
            return this;
        }

        Message.MessageBuilder withType(final Message.MESSAGE_TYPE type) {
            message.type = type;
            return this;
        }

        Message.MessageBuilder withBlocks(final List<Block> blockOlds) {
            message.blockOlds = blockOlds;
            return this;
        }

        Message.MessageBuilder withBlockchain(final Blockchain blockchain){
            message.blockchain = blockchain;
            return this;
        }


        Message.MessageBuilder withBlock(final Block block){
            message.block = block;
            return this;
        }

        Message.MessageBuilder withTransaction(final Transaction transaction){
            message.transaction = transaction;
            return this;
        }

        Message build() {
            return message;
        }

    }
}
