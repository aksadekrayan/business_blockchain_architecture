package architecture.consensus;

import logging.LoggingManager;
import architecture.p2pCommunication.CommunicationNodeManager;
import architecture.persistance.Block;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import springshell_client.ANSI_COLOR;

import java.net.InetAddress;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import static architecture.p2pCommunication.Message.MESSAGE_TYPE.SEND_NEW_BLOCK;
import static springshell_client.NodeManager.genesis;

public class ValidatorDesignationManager {

    private static int timelapse_inSeconds = 60;


    public static void run(CommunicationNodeManager nodeManager){

        long until =0;
        NTPUDPClient client = new NTPUDPClient();
        InetAddress hostAddr = null;
        try {
            hostAddr = InetAddress.getByName("pool.ntp.org");
            TimeInfo info = client.getTime(hostAddr);
            info.computeDetails(); // compute offset/delay if not already done
            Long offsetValue = info.getOffset();
            Long delayValue = info.getDelay();
            String delay = (delayValue == null) ? "N/A" : delayValue.toString();
            String offset = (offsetValue == null) ? "N/A" : offsetValue.toString();

            long current_time = info.getReturnTime();
            Calendar current_calendar = Calendar.getInstance();
            current_calendar.setTimeInMillis(current_time);

            Calendar time_calendar = Calendar.getInstance();

            time_calendar.set(Calendar.YEAR,current_calendar.get(Calendar.YEAR));
            time_calendar.set(Calendar.MONTH,current_calendar.get(Calendar.MONTH));
            time_calendar.set(Calendar.DAY_OF_MONTH,current_calendar.get(Calendar.DAY_OF_MONTH));
            time_calendar.set(Calendar.HOUR_OF_DAY,current_calendar.get(Calendar.HOUR_OF_DAY));
            time_calendar.set(Calendar.MINUTE,current_calendar.get(Calendar.MINUTE) + 1);
            time_calendar.set(Calendar.SECOND,0);
            time_calendar.set(Calendar.MILLISECOND,0);

            until = time_calendar.getTimeInMillis() - current_calendar.getTimeInMillis();

        } catch (Exception e) {
            System.err.println("Problem with network. Please try again");
           // e.printStackTrace();
        }

        client.close();
        Timer t = new Timer();
        long delay_to_minute = 0;

        ConsensusManager consensusManager = new ConsensusManager();
        t.scheduleAtFixedRate(
                new TimerTask()
                {
                    public void run()
                    {
                        //LoggingManager.log(" "+timelapse_inSeconds+" seconds passed, trying to make a new block...",nodeManager.getNode());
                        ConsensusManager consensusManager = new ConsensusManager();
                        Block blockToSend = consensusManager.processNomination(nodeManager.getNode());
                        if (blockToSend!=null){
                            if (!blockToSend.equals(genesis)) {
                                LoggingManager.log(ANSI_COLOR.GREEN+" Validator turn detected"+ANSI_COLOR.RESET, nodeManager.getNode());
                                nodeManager.broadcast(SEND_NEW_BLOCK, blockToSend);
                            }//else
                                //LoggingManager.log(" No pending transactions to add to Block",nodeManager.getNode());
                        }else{
                            //LoggingManager.log(" Not validator",nodeManager.getNode());
                        }
                    }
                },
                until,//1000,
                timelapse_inSeconds * 1000);
    }

}
