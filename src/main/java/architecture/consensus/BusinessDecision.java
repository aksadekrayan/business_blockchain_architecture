package architecture.consensus;


import logging.LoggingManager;
import architecture.persistance.Blockchain;
import architecture.persistance.Transaction;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by SNTF on 16/05/2018.
 */
public class BusinessDecision implements Serializable {

    Transaction transaction;
    Blockchain blockchain;


    public boolean decide(Transaction transaction, Blockchain blockchain){
        if (caracterisationMatch(transaction,blockchain)){
            return automaticDecision(transaction,blockchain);
        }else{
            return manualDecision();
        }
    }

    private boolean manualDecision() {
        char c=0;
        while (c!= 'y' && c!= 'Y' && c!= 'n' && c!= 'N'){
            if (c!= '\n')
                System.out.print("Manual Business Decision? Y=yes , N=no : ");
            try {
                c= (char) System.in.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (c != '\n'&& c!= 'y' && c!= 'Y' && c!= 'n' && c!= 'N')
                System.err.println("Input error, please try again");
        }
        return (c=='y'||c=='Y');
    }

    private boolean automaticDecision(Transaction transaction,Blockchain blockchain) {

        //Appels métier vers le système d'information du collaborateur
        LoggingManager.log(" Business decision handled automatically");
        return true;
    }

    private boolean caracterisationMatch(Transaction transaction,Blockchain blockchain) {
        return false;
    }

}
