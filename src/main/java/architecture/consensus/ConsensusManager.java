package architecture.consensus;


import architecture.security.*;
import architecture.security.SecurityManager;
import logging.LoggingManager;
import architecture.p2pCommunication.Message;
import architecture.p2pCommunication.Node;
import architecture.persistance.Block;
import architecture.persistance.ExecutionTransaction;
import architecture.persistance.PublicationTransaction;
import architecture.persistance.Transaction;
import springshell_client.ANSI_COLOR;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static springshell_client.NodeManager.genesis;


public class ConsensusManager {

    public static PKIAdapter pkiAdapter = PKIAdapterLauncher.launch();
    private static List<PublicationTransaction> signatureTrace = new ArrayList<>();
    private static List<PublicationTransaction> refuseTrace = new ArrayList<>();

    public Transaction processTransaction(Node node, Transaction transaction) {
        if (transaction instanceof PublicationTransaction) {
            return processPublicationTransaction(node, (PublicationTransaction) transaction);
        }else{
            return processExecutionTransaction(node, (ExecutionTransaction) transaction);
        }
    }

    public Transaction addToPendingTransactions(Node node, Transaction transaction) {
        if (!node.alreadyAdded(transaction)) {
            if (transaction.isValid()) {
                LoggingManager.log(ANSI_COLOR.GREEN+" Transaction added to pending list."+ANSI_COLOR.RESET,node);
                node.addValidTransaction(transaction);
                transaction.setNextMessageType(Message.MESSAGE_TYPE.SEND_SIGNED_TRANSACTION);
            }
        }else {
            LoggingManager.log(" Transaction already in pending list.",node);
            transaction.setNextMessageType(null);
        }
        return transaction;
    }

    private PublicationTransaction processPublicationTransaction(Node node, PublicationTransaction transaction){
        LoggingManager.log(" Transaction recieved.",node);
        boolean signedByMe= false;
        if (verifyClientIdentity(transaction)) {
            if (!node.alreadyAdded(transaction)) {
                // Si le noeud n'a pas rajouté la transaction à sa liste de transactions valides en attente
                if (transaction.containsCollaborator(node.getWallet().getPublicKey())) {
                    // Si le noeud est concerné par la collaboration
                    if (node.alreadySigned(transaction) || refuseTrace.contains(transaction)) {
                        // Si le noeud a déjà signé cette transaction
                        LoggingManager.log(" Transaction already handled", node);
                    } else {
                        // Sinon i.e. le noeud n'a jamais signé cette transaction
                        transaction.displaySmartContract(node.getWallet().getPublicKey(),node.getWallet().getPrivateKey());
                        if (node.getBusinessDecision().decide(transaction, node.getBlockchain())) {
                            if (transaction.addCollaboratorSignature(node.getWallet().getPublicKey(), node.getWallet().getPrivateKey())) {
                                // Si la signature a réussi
                                LoggingManager.log(ANSI_COLOR.GREEN+" Transaction signature successful"+ANSI_COLOR.RESET, node);
                                signedByMe = true;
                            }
                        } else {
                            refuseTrace.add(transaction);
                            //transaction.refuse(node.getWallet().getPublicKey(), node.getWallet().getPrivateKey());
                        }
                    }
                    if (transaction.isFullySigned()) {
                        // Si la transaction est totalement signée elle est rajoutée à la liste des transactions valides en attente
                        LoggingManager.log(ANSI_COLOR.GREEN+" Transaction detected as fully signed"+ANSI_COLOR.RESET, node);
                        addToPendingTransactions(node, transaction);
                        // Le noeud prépare le message à relayer aux autres noeuds sur le réseau
                        transaction.setNextMessageType(Message.MESSAGE_TYPE.SEND_SIGNED_TRANSACTION);
                    } else {
                        //Si la transaction n'est pas encore totalement signée
                            if (signedByMe || (detectNewSignature(transaction) && !refuseTrace.contains(transaction))) {
                            // Sinon, i.e. la transaction n'est pas complête, on vérifie si elle contient de nouvelles signatures
                            //      Si elle contient de nouvelles signatures, on la diffuse sur le réseau
                            addSignatureTrace(transaction);
                            transaction = signatureTrace.get(signatureTrace.indexOf(transaction));
                            transaction.setNextMessageType(Message.MESSAGE_TYPE.SEND_NEW_TRANSACTION);
                            } else {
                            transaction.setNextMessageType(null);
                            }
                    }
                } else {
                    //Si le noeud n'est pas un collaborateur
                    if (detectNewSignature(transaction)){
                        LoggingManager.log(" Broadcasting transaction to peers because of new signatures", node);
                        transaction.setNextMessageType(Message.MESSAGE_TYPE.SEND_NEW_TRANSACTION);
                        addSignatureTrace(transaction);
                        node.removePassivelyProcessedTransaction(transaction);
                    }else{
                        if (node.alreadyPassivelyProcessed(transaction)){
                            LoggingManager.log(" Transaction already passively processed", node);
                            transaction.setNextMessageType(null);
                        }else{
                            LoggingManager.log(" Broadcasting transaction to peers because never processed", node);
                            transaction.setNextMessageType(Message.MESSAGE_TYPE.SEND_NEW_TRANSACTION);
                            node.addPassivelyProcessedTransaction(transaction);
                        }
                    }
                }

            } else {
                // Si la transaction est valide et déjà en attente chez le noeud.
                LoggingManager.log(" Transaction already in pending list", node);
                transaction.setNextMessageType(null);
            }
        }else{
            // La signature du client ne peut être vérifiée ou est incorrecte
            LoggingManager.logError(" Transaction signature invalid", node);
            transaction.setNextMessageType(null);
        }
        return transaction;
    }

    private boolean verifyClientIdentity(PublicationTransaction transaction) {

        String identity = transaction.getSenderIdentity();
        String signature = transaction.getSenderSignature();
        PublicKey key_from_transaction = transaction.getSenderAddress();
        pkiAdapter = PKIAdapterLauncher.launch();
        PublicKey key_from_pki =  pkiAdapter.getPublicKey(identity);

        return  (key_from_transaction.equals(key_from_pki) &&
                SecurityManager.verifySignature(transaction.calculateHash(),signature,key_from_pki));
    }

    private void addSignatureTrace(PublicationTransaction transaction){
        if (signatureTrace.contains(transaction)){
            PublicationTransaction oldTransaction = signatureTrace.get(signatureTrace.indexOf(transaction));
            signatureTrace.add(signatureTrace.indexOf(transaction),
                    oldTransaction.fusion(transaction));
        }else{
            signatureTrace.add(transaction);
        }
    }

    private boolean detectNewSignature(PublicationTransaction transaction) {

        if (signatureTrace.isEmpty() || !signatureTrace.contains(transaction)){
            return true;
        }

        Map<PublicKey,String> newSet = transaction.getSignatures();
        Map<PublicKey,String> oldSet = signatureTrace.get(signatureTrace.indexOf(transaction)).getSignatures();

        if (newSet.size() > oldSet.size())
            return true;

        for (PublicKey key1:newSet.keySet()
             ) {
            if (!oldSet.containsKey(key1))
                return true;
        }

        return false;
    }

    private ExecutionTransaction processExecutionTransaction(Node node, ExecutionTransaction transaction){
        return transaction;
    }

    public Boolean processBlock(Node node, Block block) {
        try {
            //Si le block a déja été traité
            if (node.alreadyAdded(block)){
                LoggingManager.log(" Couldn't add Block because already added",node);
                return false;
            }

            //Lorsqu'un noeud reçoit un Block il vérifie sa validité
            if (block.isValid(node.getBlockchain().getLatestBlock())) {
                //Il rajoute le bloc à sa copie locale de la Blockchain
                node.getBlockchain().addBlock(block);
                //Il met à jour
                node.updatePendingTransactions(block.getTransactions());

                return true;
            }else {
                LoggingManager.logError(" Couldn't add Block because invalid",node);
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public Block processNomination(Node node){
        if (!node.getPendingTransactions().isEmpty()){
            //Verifie si il est vraiment validateur
            if (checkIfValidator(node)) {
                Block blockToSend = new Block(node.getWallet().getPublicKey(), new ArrayList(),
                        node.getBlockchain().getLatestBlock().getCurrentHash());

                blockToSend.sign(node.getWallet().getPrivateKey());

                List<Transaction> transactionsToAdd = node.getPendingTransactions();
                for (Transaction t : transactionsToAdd
                        ) {
                    blockToSend.addTransaction(t);
                }
                return blockToSend;
            }else{
                return null;
            }
        }else {
            return genesis;
        }

    }

    public boolean checkIfValidator(Node node) {

        int token = pkiAdapter.getToken(node.getWallet().getPublicKey());

        int block_number = node.getBlockchain().size()+1;
        int validators_number = pkiAdapter.getValidatorsNumber();
        int modulo;
        if (validators_number != 0)
            modulo = block_number % validators_number;
        else
            return false;
        //System.out.println("le token :"+token+" calculating " + block_number +"%" +validators_number + "="+modulo);

        return (modulo == token);
    }
}
