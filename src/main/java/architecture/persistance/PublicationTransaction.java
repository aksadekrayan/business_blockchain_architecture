package architecture.persistance;

import architecture.security.CollabKeyDisposer;
import architecture.security.HashUtil;
import architecture.security.SecurityManager;
import springshell_client.ANSI_COLOR;

import javax.crypto.SecretKey;
import java.io.Serializable;
import java.security.Key;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

public class PublicationTransaction extends Transaction implements Serializable {

    private String secretData;
    private String publicData;
    private String smartContract;
    private Map<PublicKey,String> collaborateurs = new HashMap<>();
    private Map<PublicKey,String> signatures = new HashMap<>();
    private CollabKeyDisposer keyDisposer ;

    public PublicationTransaction(String senderName, PublicKey senderAddress, String smartContrat) {
        super(senderAddress);
        keyDisposer = new CollabKeyDisposer();
        collaborateurs = new HashMap();
        this.smartContract = keyDisposer.encryptWithCollabKey(smartContrat);
        this.setSenderIdentity(senderName);
    }



    public String getSmartContrat() {
        return smartContract;
    }

    public void setSmartContrat(String smartContrat) {
        this.smartContract = smartContrat;
    }


    @Override
    public String calculateHash() {
        return  HashUtil.applySha256(index+secretData+smartContract+publicData+collaborateurs.toString());
    }

    @Override
    public String generateSignature(PrivateKey privatekey) {
            return (SecurityManager.sign(this.calculateHash(), privatekey));

    }

    public boolean VerifySignatures() {
        Boolean result = true;
        for (PublicKey key:collaborateurs.keySet()
             ) {
            String signature = signatures.get(key);
            if (signature == null) {
                return false;
            }
            if (!SecurityManager.verifySignature(this.calculateHash(),signature,key)){
                result = false;
                break;
            }
        }
        return result;
    }

    public boolean addCollaboratorSignature(PublicKey publicKey, PrivateKey privateKey){

        //Verifier SI le signataire fait partie des collaborateurs
        if (collaborateurs.containsKey(publicKey)){
            //Signe le message
            signatures.put(publicKey,generateSignature(privateKey));
            return true;
        }else
            return false;
    }
    public boolean addCollaborator(PublicKey publicKey){
        if (!collaborateurs.keySet().contains(publicKey)){
            Map.Entry<PublicKey,String> entry = keyDisposer.generateCollabEntry(publicKey);
            collaborateurs.put(entry.getKey(),entry.getValue());
            return true;
        }else
            return false;
    }

    public boolean containsCollaborator(PublicKey collaborator_PK){
        return collaborateurs.containsKey(collaborator_PK);
    }

    @Override
    public String toString() {
        return String.valueOf(index);
    }

    @Override
    public boolean isValid() {
        return VerifySignatures();
    }

    @Override
    public void refuse(PublicKey publicKey, PrivateKey privateKey) {
        signatures.put(publicKey,generateRefusalSignature(privateKey));
    }

    private String generateRefusalSignature(PrivateKey privateKey) {
        return SecurityManager.sign(this.calculateRefusedHash(), privateKey);
    }

    private String calculateRefusedHash() {
        return HashUtil.applySha256(index+secretData+smartContract+publicData+collaborateurs.toString() + false);
    }

    public boolean isFullySigned() {
        return collaborateurs.size()==signatures.size();
    }

    public String getSecretData() {
        return secretData;
    }

    public void setSecretData(String secretData) {
        this.secretData = secretData;
    }

    public String getPublicData() {
        return publicData;
    }

    public void setPublicData(String publicData) {
        this.publicData = publicData;
    }

    public String getSmartContract() {
        return smartContract;
    }

    public void setSmartContract(String smartContract) {
        this.smartContract = smartContract;
    }

    public Map<PublicKey, String> getCollaborateurs() {
        return collaborateurs;
    }

    public void setCollaborateurs(Map<PublicKey, String> collaborateurs) {
        this.collaborateurs = collaborateurs;
    }

    public Map<PublicKey,String> getSignatures() {
        return signatures;
    }

    public void setSignatures(Map<PublicKey,String> signatures) {
        this.signatures = signatures;
    }

    public PublicationTransaction fusion(PublicationTransaction transaction) {
        if (!this.signatures.equals(transaction.getSignatures())){
            Map newMap = new HashMap();
            newMap.putAll(transaction.getSignatures());
            newMap.putAll(this.signatures);
            this.signatures = newMap;
        }
        return this;
    }

    public boolean isRefused() {
        Boolean result = false;
        for (PublicKey key:collaborateurs.keySet()
                ) {
            String signature = signatures.get(key);
            if (!SecurityManager.verifySignature(this.calculateRefusedHash(),signature,key)){
                result = true;
                break;
            }
        }
        return result;
    }

    public void displaySmartContract(PublicKey nodePK , PrivateKey nodeSK) {
        String key = SecurityManager.decrypt(collaborateurs.get(nodePK),nodeSK);

        System.out.println("\n***********************START OF SMART-CONTRACT*************************\n");
        System.out.println(SecurityManager.decrypt(smartContract,key));
        System.out.println("\n***********************END OF SMART-CONTRACT*************************\n");
    }

    public void displayTransaction()
    {
        System.out.println(ANSI_COLOR.YELLOW+"\nTransaction \n{"+ANSI_COLOR.RESET);
        System.out.println("\n    ***********************START OF SMART-CONTRACT*************************");
        if(this.smartContract!= null)
        {
            System.out.println("    "+this.smartContract.toString());
        }
        System.out.println("    ***********************END OF SMART-CONTRACT*************************\n");
        if(this.secretData != null)
            System.out.println(ANSI_COLOR.BLUE+"    [ secret Data = "+ANSI_COLOR.RESET+this.secretData.toString()+ANSI_COLOR.BLUE+" ]");
        if(this.publicData != null)
            System.out.println(ANSI_COLOR.BLUE+"    [ Public Data = "+ANSI_COLOR.RESET+this.publicData.toString()+ANSI_COLOR.BLUE+" ]");
        System.out.println(ANSI_COLOR.YELLOW+"\n    Collaborators signatures {\n"+ANSI_COLOR.RESET);
        for (PublicKey key:this.collaborateurs.keySet()){

            if(signatures.containsKey(key))
            {
                System.out.println(ANSI_COLOR.CYAN+"        Collaborator public Key : "+ANSI_COLOR.RESET+collaborateurs.get(key).toString()+ANSI_COLOR.RESET);
                System.out.println(ANSI_COLOR.CYAN+"        Signature : "+ANSI_COLOR.RESET+signatures.get(key).toString()+"\n"+ANSI_COLOR.RESET);
            }
        }
        System.out.println(ANSI_COLOR.YELLOW+"    }");
        System.out.println("}"+ANSI_COLOR.RESET);
    }
}
