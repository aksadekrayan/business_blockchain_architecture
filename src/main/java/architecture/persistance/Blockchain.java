package architecture.persistance;
import springshell_client.ANSI_COLOR;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Blockchain implements Serializable, Sendable {

    private List<Block> blockChain;
    private int validatorsNumber;


    public Blockchain(){
        super();
        this.blockChain = new ArrayList<>();
    }
    public Blockchain(List<Block> blocks){
        super();
        this.blockChain = blocks;
    }

    //Vérifier la validité de la Blockchain
    public boolean isBlockChainValid() {
        if (blockChain.size() > 1) {
            for (int i = 1; i <= blockChain.size()-1; i++) {
                Block currentBlock = (Block) blockChain.get(i-1);
                Block nextBlock = (Block) blockChain.get(i);
                if (!(nextBlock.previousHash.equals(currentBlock.currentHash))) {
                    return false;
                }
            }
        }
        return true;
    }

    //Ajouter un nouveau Bloc à la Blockchain
    public void addBlock(Block b) {
        if (isBlockValid(b)) {
            blockChain.add(b);
        }
    }

    //Retourner la liste de tous les blocs de la Blockchain
    public List<Block> getBlocks()
    {
        return blockChain;
    }

    // retrouner le bloc N° i de la Blockchain
    public Block getBlock(int i)
    {
        return blockChain.get(i);
    }

    //Retourne le dernier Bloc de la Blockchain
    public Block getLastBlock()
    {
        return blockChain.get(blockChain.size()-1);
    }

    // Retourne la taille de la Blockchain
    public int size()
    {
        return blockChain.size();
    }

    //Afficher la Blockchain
    public void afficherBlockchain()
    {

        System.out.println(ANSI_COLOR.YELLOW+"Blockchain"+ANSI_COLOR.RESET+"\n{");
        for(Block b : blockChain) {
            System.out.println(ANSI_COLOR.YELLOW+"    Block_"+blockChain.indexOf(b)+ANSI_COLOR.RESET+"\n   {");
            System.out.println(ANSI_COLOR.CYAN+"        TimeStamp="+ANSI_COLOR.RESET + b.getTimeStamp());
            System.out.println(ANSI_COLOR.CYAN+"        Hash="+ANSI_COLOR.RESET  + b.currentHash + "");
            System.out.println(ANSI_COLOR.CYAN+"        PreviousHash="+ANSI_COLOR.RESET  + b.previousHash + "");
            if (!b.isGenesis()){
                b.afficherTransactions();
                System.out.println(ANSI_COLOR.CYAN+"        ValidatorPublicKey=\n" +ANSI_COLOR.RESET+ b.getCreatorIdentity().toString()
                        .replace("Sun","\t\t\tSun")
                        .replace("modulus","\t\t\tmodulus")
                        .replace("public","\t\t\tpublic"));
                System.out.println(ANSI_COLOR.CYAN+"        ValidatorSignature="+ANSI_COLOR.RESET + b.getCreatorSignature());

            }
            System.out.println("    }");
        }
        System.out.println("}");
    }

    @Override
    public String toString(){
        String s="";
        for(Block b : blockChain) {
            s = s + ("{\nCurentHash="+b.currentHash+"");
            s = s + ("PreviousHash="+b.previousHash+"");
            s = s + b.toString();
            s = s + ("TimeStamp="+b.getTimeStamp()+"\n}\n");
        }
        return s;
    }

    public Block getLatestBlock() {
        if (size()==0) {
            return null;
        }
        return getBlock(size() - 1);
    }



    private boolean isBlockValid(final Block block) {
        final Block latestBlock = getLatestBlock();

        if (latestBlock == null) {

            if (block.isGenesis())
                return true;
            else
                return false;
        }
        if (!Objects.equals(block.getPreviousHash(), latestBlock.getCurrentHash())) {
            System.out.println("Unmatched hash code");
            return false;
        }

        return true;
    }

    public String getStringResume() {
        return String.valueOf(blockChain.size()) + " blocks ";
    }

    public int getValidatorsNumber() {
        return validatorsNumber;
    }
}