package architecture.persistance;


import architecture.security.HashUtil;

import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import architecture.security.SecurityManager;

public class Block  implements Serializable , Sendable {

    public String currentHash;
    public String previousHash;
    private List<Transaction> transactions = new ArrayList<Transaction>();;
    private long timeStamp;

    private PublicKey creatorIdentity;
    private String creatorSignature;


    public Block(String previousHash , String currentHash) { // Seulement pour le Block genesis
        this.previousHash = previousHash;
        this.timeStamp = 0;
        this.currentHash = HashUtil.applySha256(currentHash);
    }


    public Block(PublicKey creatorIdentity, List<Transaction> transactions, String previousHash) {

        this.transactions = transactions;
        this.previousHash = previousHash;
        this.timeStamp = new Date().getTime();
        this.creatorIdentity = creatorIdentity;
        this.currentHash = this.calculateHash();
    }

    public void sign(PrivateKey key){
        this.creatorSignature= SecurityManager.sign(this.getCurrentHash(),key);
    }


    public String getCurrentHash() {
        return currentHash;
    }



    public void setCurrentHash(String currentHash) {
        this.currentHash = currentHash;
    }



    public String getPreviousHash() {
        return previousHash;
    }



    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }



    public long getTimeStamp() {
        return timeStamp;
    }


    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    //Ajouter une nouvelle transaction à un bloc
    public void addTransaction(Transaction transation)
    {
        transactions.add(transation);
    }

    //Calculer le Haché du bloc
    public String calculateHash() {
        return HashUtil.applySha256( previousHash +Long.toString(timeStamp) + transactions + creatorIdentity);

    }

    public void afficherTransactions() {
        System.out.println("\t\tTransactions=");
        if(transactions.size()>=1)
        {
            System.out.println("\t\t{");
            for(Transaction t : transactions) {
                System.out.println("\t\t\tTransaction_"+transactions.indexOf(t)+" : TimeStamp="+t.getTimestamp());
            }
            System.out.println("\t\t}");
        }
    }

    @Override
    public String toString(){
        String s="hash="+getCurrentHash()+"\nTransactions : \n";
        if(transactions.size()>=1)
        {
            for(Transaction t : transactions) {
                s = s + ("\t{\n\tsenderAddress="+t.getSenderAddress());
                s = s + ("\tTimeStamp="+t.getTimestamp()+"\n\t}\n");
            }
        }
        return s;
    }


    public boolean isValid(Block latestBlock) {

        return   (
            //Verifier le chainage avec le haché du bloc précédent
                (latestBlock.getCurrentHash().equals(this.getPreviousHash())) &&
            //Vérifier la signature du validateur
                (SecurityManager.verifySignature(getCurrentHash(),this.creatorSignature,this.creatorIdentity)) &&
            //Vérifier la validité des transactions
                (allTransactionsAreValid())
            );

    }

    private boolean allTransactionsAreValid() {
        boolean res =true;
        if (transactions.isEmpty()){
            res = false;
        }
        else
            for (Transaction t : transactions
                 ) {
                res = res && t.isValid();
            }
        return res;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public boolean isGenesis() {
        return getCurrentHash().equals(HashUtil.applySha256("genesis"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Block block = (Block) o;

        if (currentHash != null ? !currentHash.equals(block.currentHash) : block.currentHash != null) return false;
        return creatorSignature != null ? creatorSignature.equals(block.creatorSignature) : block.creatorSignature == null;
    }

    @Override
    public int hashCode() {
        int result = currentHash != null ? currentHash.hashCode() : 0;
        result = 31 * result + (creatorSignature != null ? creatorSignature.hashCode() : 0);
        return result;
    }

    public PublicKey getCreatorIdentity() {
        return this.creatorIdentity;
    }

    public String getCreatorSignature() {
        return this.creatorSignature;
    }
}