package architecture.persistance;

import java.security.PrivateKey;
import java.security.PublicKey;

public class ExecutionTransaction extends Transaction {


    private PublicKey recieverAddress;

    public ExecutionTransaction(PublicKey senderAddress, PublicKey reciever) {
        super(senderAddress);
        this.recieverAddress = reciever;
    }

    @Override
    public String calculateHash() {
        return null;
    }

    @Override
    public String generateSignature(PrivateKey privatekey) {
        return null;
    }


    @Override
    public boolean VerifySignatures() {
        return false;
    }


    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public void refuse(PublicKey publicKey, PrivateKey privateKey) {

    }
}
