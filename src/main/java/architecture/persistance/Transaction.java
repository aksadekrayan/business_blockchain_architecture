package architecture.persistance;

import architecture.p2pCommunication.Message;

import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;

public abstract class Transaction implements Serializable, Sendable {

    protected int index;
    private String senderIdentity;
    private PublicKey  senderAddress;

    public String getSenderIdentity() {
        return senderIdentity;
    }

    public void setSenderIdentity(String senderIdentity) {
        this.senderIdentity = senderIdentity;
    }

    public String getSenderSignature() {
        return senderSignature;
    }

    public void setSenderSignature(String senderSignature) {
        this.senderSignature = senderSignature;
    }

    private String senderSignature;
    private long timestamp;

    private Message.MESSAGE_TYPE nextMessageType;

    public SecurityManager securityManager;

    public Transaction(PublicKey senderAddress ) {
        this.senderAddress = senderAddress;
        this.timestamp = new Date().getTime();
    }

    public void setNextMessageType(Message.MESSAGE_TYPE type){ nextMessageType = type;}

    public Message.MESSAGE_TYPE getNextMessageType() { return nextMessageType; }

    public PublicKey getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(PublicKey senderAddress) {
        this.senderAddress = senderAddress;
    }


    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    //Calculer le Haché de la transaction
    public abstract String calculateHash();

    // Generer la signature de la transaction  (signature du haché de la transaction)
    public abstract String generateSignature(PrivateKey privatekey);

    //Vérifier la validité de la signature du haché de la transaction
    public abstract boolean VerifySignatures();

    @Override
    public abstract String toString();

    public abstract boolean isValid();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        if (timestamp != that.timestamp) return false;
        return senderAddress != null ? senderAddress.equals(that.senderAddress) : that.senderAddress == null;
    }

    @Override
    public int hashCode() {
        int result = senderAddress != null ? senderAddress.hashCode() : 0;
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        return result;
    }

    public abstract void refuse(PublicKey publicKey, PrivateKey privateKey);
}